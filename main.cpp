//
// Created by iagorosa on 23/10/18.
//

#include <cstdlib>
#include <random>
#include "Estrategia_Evolutiva.h"
#include "estrutura.h"
#include <vector>
#include <fstream>
#include <string>

Estrutura* pont;

int sign = 0;

int num_grupos;
int num_vertices;
double** W_arestas;
double* W_vertices;
double* Min;
double* Max;
int dim;
void imprime(double* X);
void verificacao(double* X);

double *buscaLocal(double *X, int dim, int *g = NULL);
double *buscaLocal2(double *X, int dim, int qtdTrocas);


void escreveArquivo(int pInt[4], int sucesso[4], vector<double> pVector[4]);

double criterio(double* X, int dim){

    double valor = 0;
    double deu_merda = 0;
    double peso_grupo;
    int inicio_grupo = num_grupos;

    for(int i = 0; i < num_grupos; i++){
        peso_grupo = 0;
//        if(X[i] == 0) deu_merda = 10;
        for(int j = 0; j < X[i]; j++){
            peso_grupo += W_vertices[(int) X[inicio_grupo+j]];
            if (j < X[i]-1) {
                for (int k = j + 1; k < X[i]; k++) {
                    valor += W_arestas[(int) X[inicio_grupo + j]][(int) X[inicio_grupo + k]];
                }
            }
        }
        inicio_grupo += X[i];
        if(peso_grupo < Min[i])
            deu_merda = Min[i]-peso_grupo;
        if(peso_grupo > Max[i])
            deu_merda = peso_grupo-Max[i];
//            cout << Min[i] << " " << peso_grupo << " " << Max[i] << endl;
    }
    valor = -valor;
    if (deu_merda != 0) valor = deu_merda;
    return valor;

}

void trocaShift(double* Y, int dim){
    int temp = rand()%num_grupos;
    int temp2 = rand()%num_grupos;

//    int temp = 3;
//    int temp2 = 1;

    //TODO: Se for instancia tipo D, nao ha verificacao com y[temp] == 1
    while (Y[temp2] == 0 || Y[temp2] == 1) temp2 = rand()%num_grupos;
//        while (y[temp] == 0) temp = rand()%num_grupos;


    //TODO: obrigar a ter uma troca? fazendo temp != temp2

    if ( temp < temp2 || abs(temp2 - temp) == 1) {
        Y[temp]++;
        Y[temp2]--;
    }

    else {

        int inicio = num_grupos;

//        int trocas = temp - 1;
        int trocas = num_grupos -1;

        random_shuffle(&Y[inicio], &Y[inicio + (int) Y[0]]);

//        cout << temp << "  " << temp2 << "  " << trocas << endl;
//        imprime(Y);
//        cout << endl << endl;

        int pos = inicio;
        int posFim;

        for (int j = 0; j < trocas; j++) {
//            if (j ==  trocas - 1) {
//                random_shuffle(&Y[pos + (int) Y[j] + (int) Y[j+1]], &Y[pos + (int) Y[j] + (int) Y[j + 1] + (int) Y[j + 2]]);
//            }

            pos += (int) Y[j];
            posFim = pos+(int)Y[j+1];

            swap(Y[pos-1], Y[posFim-1]);

//            imprime(Y);
        }

        Y[temp]++;
        Y[temp2]--;

    }

}

//void mutacao(double* x, double* y, int dim){
//
//    for(int i = 0; i < dim; i++) y[i] = x[i];
//
//    int temp, temp2;
//    temp = rand()%2;
////    temp = 1;
//    if(temp){
//        trocaShift(y, dim);
//    }else{
//        temp = rand()%(dim-num_grupos);
//        temp2 = rand()%(dim-num_grupos);
//        swap(y[num_grupos+temp],y[num_grupos+temp2]);
//    }
//
//    return;
//}

void mutacao2(double* x, double* y, int dim){

    for(int i = 0; i < dim; i++) y[i] = x[i];

    int temp, temp2;
    temp = rand()%2;
//    temp = 1;
    if(sign == 0){
        trocaShift(y, dim);
    }else if (sign == 1){
        temp = rand()%(dim-num_grupos);
        temp2 = rand()%(dim-num_grupos);
        swap(y[num_grupos+temp],y[num_grupos+temp2]);
    }else if (sign == 2)
        buscaLocal(y, dim);
    else if (sign == 3)
        buscaLocal2(y, dim, 3);

//    sign++;

//    return;
}

void verificacao(double* X){

    double peso_grupo;
    int inicio_grupo = num_grupos;

    for(int i = 0; i < num_grupos; i++){
        peso_grupo = 0;
        for(int j = 0; j < X[i]; j++){
            peso_grupo += W_vertices[(int)X[inicio_grupo+j]];
        }
        cout << "Grupo :" << i  << " Limite Minimo: " << Min[i] << " Peso: " << peso_grupo << " Limite Maximo: " << Max[i] << endl;
        inicio_grupo += X[i];
    }

}

void imprime(double* X){

    int inicio_grupo = num_grupos;
    cout << "S = {";
    for(int i = 0; i < num_grupos; i++){
        cout << "{";
        for(int j = 0; j < X[i]; j++){
            cout << X[inicio_grupo+j];
            if (j < X[i]-1)
                cout << ", ";

        }
        if (i < num_grupos-1)
            cout << "}, ";
        else
            cout << "}";
        inicio_grupo += X[i];
    }
//    cout << "}\n";
    cout << "}\t" << X[dim] << endl;
}


//double *buscaLocal(double *X, int dim, clock_t t0, int *g = NULL) {
double *buscaLocal(double *X, int dim, int *g) {

    double *Y;

    if (g == NULL)
        Y = new double[dim + 1];

//    clock_t Ticks[2];
//    Ticks[0] = t0;


    int grupoOrig; //grupo que aumenta de tamanho
    int grupoDest; //grupo que diminui de tamanho

//    clock_t  t_rest = static_cast<clock_t>(25000 - t0 * 1000.0 / CLOCKS_PER_SEC);

    for (int it = 0; it < 1; it++) {

//        Ticks[1] = clock();

//        if ((Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC > t_rest && g == NULL) break;

        if (g == NULL) {
            for (int i = 0; i <= dim; i++) Y[i] = X[i];

            grupoOrig = rand() % num_grupos;
            grupoDest = rand() % num_grupos;


            //TODO: Necessario grupos diferentes?
//        while (Y[grupoOrig] < 1) grupoOrig = rand()%num_grupos;
//        while (Y[grupoDest] < 2 or grupoDest == grupoOrig) grupoDest = rand()%num_grupos;

            while (Y[grupoOrig] < 1 or Y[grupoDest] < 2 or grupoDest == grupoOrig) {
                grupoOrig = rand() % num_grupos;
                grupoDest = rand() % num_grupos;
            }
        }
        else {
            Y = X;

            grupoOrig = g[0];
            grupoDest = g[1];
        }

        bool invertido = false;

        if ( grupoOrig > grupoDest )  {
            swap(grupoOrig, grupoDest);
            invertido = true;


            if (g != NULL) {
                int auxT=num_grupos;
                for (int i = 0; i < grupoOrig; i++) auxT += (int) Y[i];
                swap(Y[auxT], Y[auxT + (int) Y[grupoOrig] -1]);
            }

            Y[grupoOrig]--;
            Y[grupoDest]++;
        }
        if (grupoDest >= num_grupos) {
            cout << "Grupo escolhido maior que a quantidade total definida!" << endl;
            return NULL;
        }

        int fimGrupoOrig = num_grupos;

        for (int i = 0; i <= grupoOrig; i++) fimGrupoOrig += (int) Y[i];

        int trocas = grupoDest - grupoOrig;

        if (g == NULL)
            random_shuffle(&Y[fimGrupoOrig - (int) Y[grupoOrig]], &Y[fimGrupoOrig + invertido]);

        int pos = fimGrupoOrig;
        int posOld;

        if (trocas > 1) {
            if (!invertido) {

                for (int j = grupoOrig + 1; j < grupoOrig + trocas; j++) {
                    if (j == grupoOrig + trocas - 1) {
                        if (g == NULL)
                            random_shuffle(&Y[pos + (int) Y[j]], &Y[pos + (int) Y[j] + (int) Y[j + 1]]);
                    }

                    pos += (int) Y[j];
                    swap(Y[fimGrupoOrig], Y[pos]);

                }
            }
            else {
                for (int j = grupoOrig + 1; j < grupoOrig + trocas; j++) {
                    if (j == grupoOrig + trocas - 1) {
                        if (g == NULL)
                            random_shuffle(&Y[pos + (int) Y[j] + 1], &Y[pos + (int) Y[j] + (int) Y[j + 1]]);
                    }
                    posOld = pos;
                    pos += (int) Y[j];

                    swap(Y[posOld], Y[pos]);
                }
                if (g != NULL) swap(Y[pos], Y[pos+1]);
            }
        }
        else{

            if (g==NULL)
                random_shuffle(&Y[fimGrupoOrig], &Y[fimGrupoOrig + (int) Y[grupoDest]]);
        }

        if (g != NULL && g[0] > g[1] && g[2] > g[1] && g[0] > g[2]) {
            int auxT = num_grupos;
            for (int i = 0; i < g[2]; i++) auxT += (int) Y[i];
            swap(Y[auxT], Y[auxT + 1]);
        }

        if (g == NULL)
            swap(Y[fimGrupoOrig - 1], Y[pos + 1]);

        if (!invertido) {
            Y[grupoOrig] ++;
            Y[grupoDest] --;
        }

        Y[dim] = criterio(Y, dim);

//        if(it%((int)(1e+5)) == 0 && Y[dim] < 0 && g == NULL) {
//            printf("%d %e ", it, X[dim]);
//            printf("\n");
//        }

        if (g != NULL) {
//            for (int i = 0; i <= dim; i++) X[i] = Y[i];
//            delete[] Y;
            return Y;
        }

        if (Y[dim] < X[dim]) {
            for (int i = 0; i <= dim; i++) X[i] = Y[i];
        }

    }

//    imprime(Y);
    delete [] Y;
//    imprime(X);
//    cout << endl;
    return X;
}

int fatorial(int x){

    int res = 1;
    for (int i = x; i > 1; i--){
        res += i;
    }
    return res;
}


//double *buscaLocal2(double *X, int dim, clock_t t0, int qtdTrocas) {
double *buscaLocal2(double *X, int dim, int qtdTrocas) {


    double *Y = new double[dim + 1];
//    double *M = new double[dim + 1];
    double *Z = new double[dim + 1];

//    for (int i = 0; i <= dim; i++) M[i] = X[i];

    double *nosTrocas = new double[qtdTrocas];
    int *grupos = new int[qtdTrocas];
    int *posicoes = new int[qtdTrocas];

    int temp;
    int i = 0, j = 0, k = 0;
    int fat = fatorial(qtdTrocas);
    int *nosTrocasOriginal = new int[qtdTrocas];

    ///////////// USADOS NA TERCEIRA PARTE //////////////
    int* gruposAux = new int[qtdTrocas];
//    int* gruposAuxOrig = new int[qtdTrocas];
//    int* posAux = new int[qtdTrocas];
    int* gruposTroca = new int[qtdTrocas];
    ////////////////////////////////////////////////////

//    imprime(X);

//    for (i = 0; i < num_grupos; i++)
//        cout << X[i] << " ";
//    cout << endl;


//    clock_t Ticks[2];
//    Ticks[0] = t0;

//    cout << "Individuo original (X): " << endl;
//    imprime(X);
//    cout << endl;

//    clock_t t_rest = static_cast<clock_t>(57000 - t0 * 1000.0 / CLOCKS_PER_SEC);

    for (int it = 0; it < 1; it++) {

//        Ticks[1] = clock();
//
//        if ((Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC > t_rest) break;

        for (i = 0; i <= dim; i++) Y[i] = X[i];

//        cout << "Melhor individuo atual (Y e X): " << endl;
//        imprime(X);
//        cout << endl;

//        if (it % ((int) (1e+5)) == 0) {
//            printf("%d %e ", it, M[dim]);
//            printf("\n");
//        }

        //inicializa os grupos com -1
        for (i = 0; i < qtdTrocas; i++) grupos[i] = -1;

        //seleciona grupos diferentes para realizar as trocas
        for (i = 0; i < qtdTrocas; i++) {
            temp = rand() % num_grupos;
            while (j < i && X[temp] != 0) {  // TODO: verificando se grupo selecionado nao eh vazio. Necessario?
                if (temp == grupos[j]) {
                    temp = rand() % num_grupos;
                    j = 0;
                } else
                    j++;
            }
            grupos[i] = temp;
            j = 0;
        }

        //ordena os grupps selecionados para facilitar
        sort(&grupos[0], &grupos[qtdTrocas]);

        //para cada grupo selecionado, embaralha-o e seleciona o no da primeira posição
        int bgn = num_grupos, end = 0, inicioIt = 0;
        for (j = 0; j < qtdTrocas; j++) {

            for (i = inicioIt; i < grupos[j]; i++) {
                bgn += (int) Y[i];
            }
            inicioIt = grupos[j];
            end = bgn + (int) Y[grupos[j]];

            random_shuffle(&Y[bgn], &Y[end]);

            nosTrocas[j] = Y[bgn];
            posicoes[j] = bgn;
        }

//        cout << "Grupos e nos selecionados para as trocas: \nGrupos: ";
//        for (k = 0; k < qtdTrocas; k++) cout << grupos[k] << "\t";
//        cout << endl;

//        cout << "Nos: ";
//        for (k = 0; k < qtdTrocas; k++) cout << nosTrocas   [k] << "\t";
//        cout << endl << endl;

        int t1 = 0, t2 = 1;

        //nosTrocasOriginal serve para fazer o Y voltar ao seu original fazendo apenas as trocas para as posicoes originais
        for (i = 0; i < qtdTrocas; i++) nosTrocasOriginal[i] = (int) nosTrocas[i];

//        cout << "Inicio da primeira parte com Y: " << endl;
//        imprime(Y);
//        cout << endl;
//        cout << "Permutação dos nos selecionados: " << endl;

        // Realiza todas as possibilidades de permutacao dos nos selecionados (nosTrocas)
        // Para cada possibilidade, inseri os nos das trocas nas primeiras posicoes dos grupos selecionados.
        for (i = 1; i < fat; i++) {

            swap(nosTrocas[t1], nosTrocas[t2]);


            for (k = 0; k < qtdTrocas; k++) Y[posicoes[k]] = nosTrocas[k];

//            cout << "oi" << endl;
            Y[dim] = criterio(Y, dim);

//            imprime(Y);

            if (Y[dim] < X[dim]) { for (k = 0; k <= dim; k++) X[k] = Y[k]; }

            t1 = (t1 + 1) % (qtdTrocas - 1);
            t2 = t1 + 1;

            for (k = 0; k < qtdTrocas; k++) Y[posicoes[k]] = nosTrocasOriginal[k];
        }

        swap(nosTrocas[t1], nosTrocas[t2]);

        delete [] posicoes;
        delete [] nosTrocasOriginal;
        delete [] nosTrocas;

        ///////////////////   SEGUNDA PARTE   ////////////////////////

//        cout << "\nInicio da segunda parte com Y: ";
//        imprime(Y);

        //Cada grupo recebe todos os nos selecionados de uma vez.
        //O processo eh realizado para todos os grupos
        for (i = 0; i < qtdTrocas; i++) {

//            for (k = 0; k < qtdTrocas; k++) cout << grupos[k] << "\t";
//            cout << endl;

            for (k = 0; k <= dim; k++) Z[k] = Y[k];

            for (j = 1; j < qtdTrocas; j++) {

                    Z = buscaLocal(Z, dim, grupos);
                if (j < qtdTrocas - 1)
                    swap(grupos[j], grupos[j + 1]);

            }
            //TODO: conferir se a ultima posicao esta realmente sendo setada com a funcao criterio dentro da funcao buscalocal()


//            imprime(Z);
            swap(grupos[0], grupos[qtdTrocas - 1]);

            if (Z[dim] < X[dim]) for (k = 0; k <= dim; k++) X[k] = Z[k];

        }

/////////////////// TERCEIRA PARTE

//        cout << "\nInicio da terceira parte com Y: ";
//        imprime(Y);

        t1 = 0, t2 = 1;
        int t[2] = {1, 2};
        int ti = 0;

        int a1, a2;

//        for (k=0; k< num_grupos; k++) gruposAuxOrig[k] = grupos[k];
        for (k=0; k< qtdTrocas; k++) gruposAux[k] = grupos[k];

        // Realiza todas as possibilidades de permutacao dos grupos selecionados
        // Para cada possibilida, a primeira posicao mantem seu proprio no e pega o no da segunda posicao
        // e o no da segunda posicao recebe o que estava na ultima posicao.
        // Para cada possibilidade sao realizadas 3 operacoes:
        // A primeira original, mantendo o que foi falado acima
        // A segunda fazendo um swap entre o no do primeiro grupo com o no do ultimo
        // A terceira fazendo um swap entre o no do segundo grupo com o no do ultimo


        for (i = 0; i < fat; i++) {

            for (k = 0; k <= dim; k++) Z[k] = Y[k];
            ti = 0;

//            for (k = 0; k < qtdTrocas; k++) gruposAux[k] = gruposAuxOrig[k];

//            for (k = 0; k < qtdTrocas; k++) cout << gruposAux[k] << "\t";
//            cout << endl;

            for(j = 0; j < qtdTrocas; j++){

                if (grupos[2] != gruposAux[0]) {

                    if (grupos[2] != gruposAux[1]) {

                        gruposTroca[0] = gruposAux[0];
                        gruposTroca[1] = gruposAux[1];
                        gruposTroca[2] = gruposAux[2];
                        Z = buscaLocal(Z, dim, gruposTroca);

                        gruposTroca[0] = gruposAux[1];
                        gruposTroca[1] = gruposAux[2];
                        gruposTroca[2] = gruposAux[0];
                        Z = buscaLocal(Z, dim, gruposTroca);

                        t[0] = 1;
                        t[1] = 2;

                    } else {

                        gruposTroca[0] = gruposAux[0];
                        gruposTroca[1] = gruposAux[2];
                        gruposTroca[2] = gruposAux[1];
                        Z = buscaLocal(Z, dim, gruposTroca);

//                        swap(gruposTroca[0], gruposTroca[1]);
//                        swap(gruposTroca[1], gruposTroca[2]);
//                        Z = buscaLocal(Z, dim, Ticks[0], gruposTroca);

                        t[0] = 2;
                        t[1] = 1;
                    }
                } else {

                    gruposTroca[0] = gruposAux[0];
                    gruposTroca[1] = gruposAux[1];
                    gruposTroca[2] = gruposAux[2];
                    Z = buscaLocal(Z, dim, gruposTroca);

                    swap(gruposTroca[0], gruposTroca[1]);
                    swap(gruposTroca[1], gruposTroca[2]);

//                    swap(gruposTroca[0], gruposTroca[2]);

                    Z = buscaLocal(Z, dim, gruposTroca);
//                    cout << "G1 <- G1 e G2: " << endl;
//                    imprime(Z);

                    t[0] = 1;
                    t[1] = 2;
                }
                if (Z[dim] < X[dim]) for (k = 0; k <= dim; k++) X[k] = Z[k];

//                imprime(Z);

                for (k = 0; k <= dim; k++) Z[k] = Y[k];


                a1 = num_grupos;
                a2 = num_grupos;
                for (k = 0; k < gruposAux[ti] && j < qtdTrocas-1; k++) a1 += Z[k];
                for (k = 0; k < gruposAux[t[1]]; k++) a2 += Z[k];

                swap(Z[a1], Z[a2]);

                ti += t[0];

            }

            t1 = (t1 + 1)  % (qtdTrocas - 1);
            t2 = t1 + 1;
//            swap(gruposAuxOrig[t1], gruposAuxOrig[t2]);
            swap(gruposAux[t1], gruposAux[t2]);

        }

//        cout << endl;

////////////////////

    }

//    imprime(X);
    delete [] Z;
    delete [] Y;

    delete [] grupos;
    delete [] gruposAux;
    delete [] gruposTroca;

//    delete [] posAux;

//    imprime(X);

//    cout << endl;
    return X;
}

double* buscaLocalGeral(double *X, int dim){

    double *Y = new double[dim+1];
    for (int i =0; i <= dim; i++) Y[i] = X[i];
    int posicao_i = num_grupos;
    int posicao_j;

//    imprime(Y);
    for(int i = 0; i < num_grupos; i++) {
        posicao_j = num_grupos;
//        posicao_i++;

        for (int v1 = 0; v1 < Y[i]; v1++) {

            for (int j = 0; j < num_grupos; j++) {
//                cout << "Grupo " << j << "/" << num_grupos-1 << " i:" << i <<" " <<posicao_i-num_grupos << " j:" << j <<" " << X[j] << " " << posicao_j-num_grupos << endl;
                if (i != j) {
//                    posicao_j++;
                    for (int v2 = 0; v2 < Y[j]; v2++) {
                        swap(Y[posicao_i], Y[posicao_j]);
                        Y[dim] = criterio(Y, dim);

//                        if (i == num_grupos-1)
//                        imprime(Y);
                        if (Y[dim] < X[dim]) {
                            for (int k = 0; k <= dim; k++) X[k] = Y[k];
                        }

                        swap(Y[posicao_i], Y[posicao_j]);
                        posicao_j++;
                    }
//                    if (i ==  num_grupos-1)
//                    cout << endl;
                }
                else{
//                    cout <<  posicao_j << " " << X[j] << " Pos: " << posicao_j + X[j] - num_grupos << endl;
                    posicao_j += X[j];
                }
            }
            posicao_j = num_grupos;
            posicao_i++;
        }
    }

    delete [] Y;
    return X;
}

void escreveArquivo(int casos[4], int sucesso[4], vector<double> ganho[4], vector<clock_t > tempos[4], double best, clock_t clk, string name) {

    ofstream arq;
    arq.open(name +".txt");

    for (int j = 0; j < 4; j++)
        arq << casos[j] << " ";
    arq << endl;

//    cout << "\nContagem dos sucessos: " << endl;
    for (int j = 0; j < 4; j++)
        arq << sucesso[j] << " ";
    arq << endl;

//    cout << "Lista de ganhos: " << endl;
    for (int j = 0; j < 4; j++) {
        for (vector<double, std::allocator<double>>::iterator k = ganho[j].begin(); k != ganho[j].end(); ++k)
            arq << *k << " ";
        arq << endl;
    }
//    arq << endl;

    for (int j = 0; j < 4; j++) {
        for (vector<clock_t , std::allocator<clock_t >>::iterator k = tempos[j].begin(); k != tempos[j].end(); ++k)
            arq << *k << " ";
        arq << endl;
    }
//    arq << endl;

    arq << clk << endl;
    arq << best << endl;

    arq.close();

}

int main(){

//    double* melhor_ind = new double[dim+1];

    string tipo;
    string numr;
    string nome;
    string grap_name;
    vector<double> *qtdGanho;
    vector<clock_t> *tempos;
    for (int tp = 1; tp < 3; tp++) {
        if(tp == 0) {
            tipo = "A";
            numr = "DCC136-82_0";
        }
        else if(tp == 1){
            tipo = "B";
            numr = "DCC136-240_0";
        }
        else if(tp == 2){
            tipo = "C";
            numr = "DCC136-480_0";
        }

        for (int inst = 4; inst <= 5; inst++) {

            for (int it = 1; it <= 5; it++) {

                pont = new Estrutura();
//        pont->leitura("../../instancias-Grupos/instancias-Grupos/TipoA/DCC136-82_01.txt", 0);
                nome = "../../instancias-Grupos/instancias-Grupos/Tipo" + tipo + "/" + numr + to_string(inst) + ".txt";
                pont->leitura(nome, tipo == "D");

                srand(static_cast<unsigned int>(it));

                grap_name = numr + to_string(inst) + "_" + to_string(it);

                num_grupos = pont->getQtdGrupos();
                num_vertices = pont->getQtdNos();
                W_arestas = pont->getMat();
                W_vertices = pont->getPesoNos();
                Min = pont->getLimites_inf();
                Max = pont->getLimites_sup();
                dim = num_grupos + num_vertices;

                int contCasos[4] = {0, 0, 0, 0};
                int contSucesso[4] = {0, 0, 0, 0};
                double criterioAnterior = 0;

                qtdGanho = new vector<double>[4];
                tempos   = new vector<clock_t>[4];

                Estrategia_Evolutiva *es = new Estrategia_Evolutiva(dim, 4, criterio, mutacao2, pont);

                for (int i = 0; i <= dim; i++) cout << es->get_melhor_individuo()[i] << "\t";
                cout << endl;

//    geracaoInicial(es->get_melhor_individuo(), dim);

                int repeticoes = 0;
                double last = es->get_melhor_criterio();

                int n = 4;

                clock_t Ticks[2];
                Ticks[0] = clock();
                for (int i = 0; i < RAND_MAX; i++) {
                    Ticks[1] = clock();
//        if ((Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC > 157000) break;

                    if (es->get_melhor_criterio() != last) {
                        last = es->get_melhor_criterio();
                        repeticoes = 0;
                    } else if (repeticoes > 3e+5) break;
                    repeticoes++;

                    if (i % ((int) 1e+3) == 0 && es->get_melhor_criterio() > 0) {
                        es->setMelhorIndividuo(es->geracaoInicial(es->get_melhor_individuo()));
                    }

                    if (i % ((int) (1e+4)) == 0) {
                        printf("%d %e ", i, es->get_melhor_criterio());
//            for (int j = 0; j < num_grupos; j++){
//                cout << " " << es->get_melhor_individuo()[j];
//            }
                        printf("\n");

//            cout << "Contagem dos casos: " << endl;
//            for (int j = 0; j < 4; j++)
//                cout << contCasos[j] << " ";
//
//            cout << "\nContagem dos sucessos: " << endl;
//            for (int j = 0; j < 4; j++)
//                cout << contSucesso[j] << " ";
//            cout << endl;
//
//            cout << "Lista de ganhos: " << endl;
//            for (int j = 0; j < 4; j++) {
//                for (vector<double, std::allocator<double>>::iterator k = qtdGanho[j].begin(); k != qtdGanho[j].end(); ++k)
//                    cout << *k << " ";
//                cout << endl;
//            }
//            cout << endl;


                    }
//                    criterioAnterior = es->get_melhor_individuo()[dim];
////                    if (i != 0 && i % ((int) (1e+3)) == 0 && repeticoes == 1){
//                    if(repeticoes == 100){
//                        es->setMelhorIndividuo(buscaLocalGeral(es->get_melhor_individuo(), dim));
////                        printf("%d %e \n", i, es->get_melhor_criterio());
//                        if (es->get_melhor_individuo()[dim] < criterioAnterior) {
//                            contCasos[0]++;
//                            contSucesso[0]++;
//                            qtdGanho[0].push_back(-(es->get_melhor_individuo()[dim] - criterioAnterior));
//                            tempos[0].push_back(Ticks[1]);
//                        }
//                    }

                    sign = rand() % n;

                    criterioAnterior = es->get_melhor_individuo()[dim];
                    contCasos[sign]++;

                    es->proxima_geracao();

                    if (es->get_melhor_individuo()[dim] < criterioAnterior) {
                        contSucesso[sign]++;
                        qtdGanho[sign].push_back(-(es->get_melhor_individuo()[dim] - criterioAnterior));
                        tempos[sign].push_back(Ticks[1]);
                    }

//        if (i > 150000) {
//            for (int j = 0; j < num_grupos; j++){
//                cout << es->get_melhor_individuo()[j] << " ";
//            }
//            cout << endl;
//        }

                }

                imprime(es->get_melhor_individuo());
//
                for (int i = 0; i < num_grupos; i++) cout << es->get_melhor_individuo()[i] << "\t";
//
                cout << endl << es->get_melhor_criterio() << endl;

                escreveArquivo(contCasos, contSucesso, qtdGanho, tempos, es->get_melhor_criterio(),
                               static_cast<clock_t>((Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC), grap_name);




/////////// BUSCA LOCAL

//    Ticks[1] = clock();
//
//    es->setMelhorIndividuo( buscaLocal(es->get_melhor_individuo(), dim, Ticks[1]) );
//
//    imprime(es->get_melhor_individuo());
//
//    for (int i = 0; i < num_grupos; i++) cout << es->get_melhor_individuo()[i] << "\t";
//
//    cout << endl << es->get_melhor_criterio() << endl;

/////////////



////////////// BUSCA LOCAL 2

//    Ticks[1] = clock();
//
//    es->setMelhorIndividuo( buscaLocal2(es->get_melhor_individuo(), dim, Ticks[1], 3) );
//
//    imprime(es->get_melhor_individuo());
//
//    for (int i = 0; i < num_grupos; i++) cout << es->get_melhor_individuo()[i] << "\t";
//
//    cout << endl << es->get_melhor_criterio() << endl;

//////////////


                verificacao(es->get_melhor_individuo());
                cout << "Tempo: " << (Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC << endl;

//    cout << endl<< "FIm";
                delete [] qtdGanho;
                delete [] tempos;
                delete pont;
                delete es;
//    delete [] melhor_ind;
            }
        }
    }

    return 0;

}
