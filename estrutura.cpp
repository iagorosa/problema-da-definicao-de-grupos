//
// Created by iagorosa on 23/10/18.
//

#include "estrutura.h"

using namespace std;

Estrutura::Estrutura() {
    qtdNos = 0, qtdArestas = 0, qtdGrupos = 0;
    mat = NULL;
    pesoNos = NULL;
//    limites = NULL;
    double *limites_inf;
    double *limites_sup;
}


void Estrutura::leitura(const string nomeArq, bool type) {
    ifstream entrada;
    entrada.open(nomeArq, ios::in);
    int n_nos, n_arestas, n_grupos, i, j;
    string tipo;

    entrada >> n_nos;
    entrada >> n_grupos;
    if (!type) entrada >> tipo;

    n_arestas = n_nos * (n_nos - 1)/2;

    // cout << n_nos << endl;

    mat = new double*[n_nos];

    for (i = 0; i < n_nos; i++)
        mat[i] = new double[n_nos];

    ///insere todos os nos
    pesoNos = new double[n_nos];

    limites_inf = new double[n_grupos];
    limites_sup = new double[n_grupos];

    double I, S;

    if (type){
        entrada >> S;
        for (i = 0; i < n_grupos; i++) {
            limites_inf[i] = 0;
            limites_sup[i] = S;
        }
    }

    else {
        for (i = 0; i < n_grupos; i++) {
            entrada >> I >> S;
            limites_inf[i] = I;
            limites_sup[i] = S;
        }
    }

    char lixo;
    if (!type) entrada >> lixo;

    double peso;
    double soma = 0;
    for (i = 0; i < n_nos; i++) {
        entrada >> peso;
        pesoNos[i] = peso;
        soma += peso;
    }

    int it = 0;
    float peso_aresta;

    if (type) {
        for (i = 0; i < n_nos; i++){
            for (j = 0; j < n_nos; j++) {
                entrada >> peso_aresta;
                mat[i][j] = peso_aresta;
            }
        }

    }

    else {
        while (it < n_arestas){
            entrada >> i >> j >> peso_aresta;
            if(i > n_nos || j > n_nos) {
                cout << "ID dos nos maior que numero de nos!" << endl;
                entrada.close();
                return;
            }
            mat[i][j] = peso_aresta;
            mat[j][i] = peso_aresta;
            it++;
        }
    }

    this->setQtdNos(n_nos);
    this->setQtdArestas(n_arestas);
    this->setQtdGruopos(n_grupos);
}


int Estrutura::getQtdNos() const {
    return qtdNos;
}

int Estrutura::getQtdArestas() const {
    return qtdArestas;
}

int Estrutura::getQtdGrupos() const {
    return qtdGrupos;
}

double **Estrutura::getMat() const {
    return mat;
}

void Estrutura::setQtdGrupos(int qtdGrupos) {
    Estrutura::qtdGrupos = qtdGrupos;
}

double *Estrutura::getPesoNos() const {
    return pesoNos;
}

double *Estrutura::getLimites_inf() const {
    return limites_inf;
}

double *Estrutura::getLimites_sup() const {
    return limites_sup;
}



