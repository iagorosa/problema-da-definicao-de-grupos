//
// Created by iagorosa on 23/10/18.
//

#ifndef GRUPOS_ESTRUTURA_H
#define GRUPOS_ESTRUTURA_H

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

class Estrutura {
    int qtdNos, qtdArestas, qtdGrupos;
    double **mat;
    double* pesoNos;
    double* limites_inf;
    double* limites_sup;

public:
    Estrutura();
    void leitura(string nome, bool tipo);

    ~Estrutura(){};

    void setQtdNos(int qtdNos){ this->qtdNos = qtdNos; }
    void setQtdArestas(int qtdArestas){ this->qtdArestas = qtdArestas; }
    void setQtdGruopos(int qtdGrupos){ this->qtdGrupos = qtdGrupos; }
    void setMatriz(double **mat){ this->mat = mat; }
    void setPesoNos(double* pesoNos){ this->pesoNos = pesoNos; }
    void setLimitesInf(double* limites){ this->limites_inf = limites; }
    void setLimitesSup(double* limites){ this->limites_sup = limites; }

    int getQtdNos() const;
    int getQtdArestas() const;
    int getQtdGrupos() const;

    double *getPesoNos() const;

    double *getLimites_inf() const;

    double *getLimites_sup() const;

    double **getMat() const;

    void setQtdGrupos(int qtdGrupos);
};


#endif //GRUPOS_ESTRUTURA_H
