#include "Estrategia_Evolutiva.h"

Estrategia_Evolutiva::Estrategia_Evolutiva(int dimensao, int lambda, double (*criterio)(double*, int), void (*mutacao)(double*, double*, int), Estrutura* est){
	this->lambda = lambda;
	this->dimensao = dimensao;
	this->mutacao = mutacao;
	this->criterio = criterio;
	this->est = est;

	individuos = new double*[1+this->lambda];
	for(int i = 0; i < 1+this->lambda; i++){
		individuos[i] = new double[1+this->dimensao];
	}
//	for(int j = 0; j < this->dimensao; j++){
//		individuos[0][j] = 2.0*(((double) rand())/RAND_MAX)-1.0;
//	}
//	individuos[0][this->dimensao] = criterio(individuos[0], this->dimensao);

	individuos[0] = geracaoInicial(individuos[0]);
}

double* Estrategia_Evolutiva::geracaoInicial(double *X){
    int n_grupos = est->getQtdGrupos();
    int n_nos = est->getQtdNos();
//    double *X;

    int i = 0;

    for (i=0; i < n_grupos; i++) X[i] = (int) n_nos/n_grupos;
//    X[0]+=n_nos%n_grupos;

    int resto = n_nos%n_grupos;
    for (i = 0; i < resto; i++) X[i]++;

    for (i=n_grupos; i < n_nos+n_grupos; i++) X[i] = i-n_grupos;

//    double C[] = {7, 7, 9, 8, 9, 15, 21, 6, 1, 25, 22, 13, 38, 0, 24, 17, 23, 10, 39, 9, 19, 29, 16, 7, 37, 11, 5, 20, 27, 34, 8, 36, 35, 3, 33, 2, 18, 31, 28, 14, 4, 32, 26, 12, 30, 1000 };

//    for (i = 0; C[i] != 1000; i++);
//    cout << "Tam: " << i-1 << endl;
//    for (i = 0; i < dimensao; i++) X[i] = C[i];

//    int temp, temp2;
//    for(int i = 0; i < 2*n_nos; i++){
//        temp = rand()%(dimensao-n_grupos);
//        temp2 = rand()%(dimensao-n_grupos);
//        swap(X[n_grupos+temp],X[n_grupos+temp2]);
//    }
//
//    for(int i = 0; i < n_nos; i++){
//        cout << X[n_grupos+i] << "\t";
//    }
//    cout << endl;

    random_shuffle(&X[n_grupos], &X[dimensao]);



    X[dimensao] = criterio(X, dimensao-1);

//    for(i = 0; i < n_nos; i++){
//        cout << X[n_grupos+i] << "\t";
//    }
//    cout << endl;
//
//    for (i = 0; i < n_grupos; i++) cout << X[i] << "\t";
//
//    cout << endl;
//
//    imprime(X);
//    cout  <<  criterio(X, dimensao) << endl;
//    cout << endl;
//    verificacao(X);
//    cout << endl << endl;

	return X;

}

Estrategia_Evolutiva::~Estrategia_Evolutiva(){
	for(int i = 0; i < 1 + this->lambda; i++){
		delete[] individuos[i];
	}	delete[] individuos;
}

void Estrategia_Evolutiva::proxima_geracao(){
	
	for(int i = 0; i < this->lambda; i++){
		this->mutacao(this->individuos[0], this->individuos[1+i], this->dimensao);
		this->individuos[1+i][this->dimensao] = this->criterio(this->individuos[1+i], this->dimensao);
		//printf("%lf\n", this->individuos[1+i][this->dimensao]);
	}
	int p = 0;
	double v = individuos[0][this->dimensao];
	for(int i = 0; i < this->lambda; i++){
		if(individuos[i+1][this->dimensao] <= v){
			//printf("%lf\t%lf\n", individuos[i+1][this->dimensao], v);
			p = i+1;
			v = individuos[i+1][this->dimensao];
		}
	}
	if(p != 0){
		swap(individuos[p], individuos[0]);
	}
	
}

double Estrategia_Evolutiva::evolucao(int numero_geracoes){
	for(int i = 0; i < numero_geracoes; i++) this->proxima_geracao();
	return this->get_melhor_criterio();
}


void Estrategia_Evolutiva::setMelhorIndividuo(double *X){
	individuos[0] = X;
}

void Estrategia_Evolutiva::setMelhorCriterio(double x){
	individuos[0][dimensao+1] = x;
}


