#ifndef ESTRATEGIA_EVOLUTIVA_H
#define ESTRATEGIA_EVOLUTIVA_H

#include <iostream>
#include "estrutura.h"

using namespace std;

class Estrategia_Evolutiva{
	
    private:
		int dimensao;
		int lambda;

		Estrutura* est;
    
		double** individuos; // [individuos 1+lambda][dimensao+1] ultima posicao com o criterio
		double (*criterio)(double*, int);
		void (*mutacao)(double*, double*, int);

    public:
        Estrategia_Evolutiva(int dimensao, int lambda, double (*criterio)(double*, int), void (*mutacao)(double*, double*, int), Estrutura* p);
        ~Estrategia_Evolutiva();

		double* get_melhor_individuo(){return individuos[0];}
		double get_melhor_criterio(){return individuos[0][this->dimensao];}

		void proxima_geracao();
		double evolucao(int numero_geracoes);

		double* geracaoInicial(double *X);

		void setMelhorIndividuo(double *X);

          void setMelhorCriterio(double x);
};

#endif // ESTRATEGIA_EVOLUTIVA_H

